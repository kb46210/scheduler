﻿using Serilog;
using System;
using System.Collections.Generic;
using System.Text;
using Serilog.Events;
using Newtonsoft.Json;
using System.IO;
using System.Threading.Tasks;
using Quartz;
using Quartz.Logging;
using System.Diagnostics;
using Quartz.Impl.Matchers;

namespace JobLibrary.Share
{
    public class Job
    {
        private IScheduler _schedular = null;
        public Job()
        {
            try
            {
                Serilog.Debugging.SelfLog.Enable(Console.Error);
                Serilog.Debugging.SelfLog.Enable(msg => Console.WriteLine(msg));
                Serilog.Debugging.SelfLog.Enable(msg => Debug.WriteLine(msg));
                //serilog log 紀錄器
                Log.Logger = new LoggerConfiguration()
                            .MinimumLevel.Information()
                            .WriteTo.File($@"{Config.Path}/logs/log_{DateTime.Now:yyyyMMdd}.txt", retainedFileCountLimit: 7)
                            .CreateLogger();
                
                // LogProvider.SetCurrentLogProvider(new QuartzLogProvider());
            }
            catch (Exception e)
            {
                Log.Error(e.ToString());
            }
        }
        public void Start()
        {
            try
            {
                Log.Information("##Quzrtz Start##");
                RunSchedule().GetAwaiter().GetResult();
            }
            catch (Exception e)
            {
                Log.Error(e.ToString());
            }
        }
        public void Stop()
        {
            try
            {
                Log.Information("##Quzrtz Stop##");
                _schedular.Shutdown();
                Log.CloseAndFlush();
            }
            catch (Exception e)
            {
                Log.Error(e.ToString());
            }
        }
        public List<JobConfig> GetJsonConfig()
        {
            return JsonConvert.DeserializeObject<List<JobConfig>>(File.ReadAllText(Config.Path + "/Config/JobConfig.json"));
        }
        /// <summary>
        /// 掃描config執行job
        /// </summary>
        /// <returns></returns>
        private async Task RunSchedule()
        {
            var schedulerFactory = new Quartz.Impl.StdSchedulerFactory();
            _schedular = await schedulerFactory.GetScheduler();
            _schedular.ListenerManager.AddJobListener(new JobListener(), GroupMatcher<JobKey>.AnyGroup());
            await _schedular.Start();
            var jobs = this.GetJsonConfig();
            foreach (var job in jobs)
            {
                var result = AddJob(job.ClassName, job.Cron);
                if (result != null)
                    await _schedular.ScheduleJob(result.Item1, result.Item2);
            }
            

        }
        private Tuple<IJobDetail, ITrigger> AddJob(string clsName, string cron)
        {
            IJobDetail _job = null;
            ITrigger _trigger = null;
            try
            {
                var type = Type.GetType(clsName);
                _job = JobBuilder.Create(type)
                           .WithIdentity(clsName)
                           .Build();
                _trigger = TriggerBuilder.Create()
                                .WithCronSchedule(cron)
                                .WithIdentity(clsName)
                                .Build();
                return Tuple.Create(_job, _trigger);
            }
            catch (Exception e)
            {
                Log.Error(e.ToString());
            }
            return null;
        }
    }
    public class JobConfig
    {
        public string ClassName { get; set; }
        public string Cron { get; set; }
    }
}
