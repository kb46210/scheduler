﻿using Quartz;
using Serilog;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace JobLibrary.Share
{
    public class JobListener : IJobListener
    {
        public string Name {
            get
            {
                return "job";
            }
        }

        public Task JobExecutionVetoed(IJobExecutionContext context, CancellationToken cancellationToken = default)
        {
            return Task.CompletedTask;
        }

        public Task JobToBeExecuted(IJobExecutionContext context, CancellationToken cancellationToken = default)
        {
            Task.Run(() => { Log.Information("★★★★★★★★★★★" + context.JobDetail.JobType.ToString() + " Start ★★★★★★★★★★★"); });
            return Task.CompletedTask;
        }

        public Task JobWasExecuted(IJobExecutionContext context, JobExecutionException jobException, CancellationToken cancellationToken = default)
        {
            Task.Run(() => { Log.Information("×××××××××××××" + context.JobDetail.JobType.ToString() + " End ×××××××××××××××"); });
            return Task.CompletedTask;
        }
    }
}
