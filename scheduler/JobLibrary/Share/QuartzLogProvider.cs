﻿using Quartz.Logging;
using Serilog;
using System;
using System.Collections.Generic;
using System.Text;

namespace JobLibrary.Share
{
    class QuartzLogProvider : ILogProvider
    {
        public Logger GetLogger(string name)
        {
            return (level, func, exception, para) =>
            {
                if (func != null)
                    switch (level)
                    {
                        //case LogLevel.Debug:
                        //    Log.Debug(func(), para); break;
                        case LogLevel.Error:
                            Log.Error(func(), para); break;
                        case LogLevel.Fatal:
                            Log.Fatal(func(), para); break;
                            //case LogLevel.Info:
                            //    Log.Information(func(), para); break;
                            //default:
                            //    Log.Information($@"[{level.ToString()}] {func()}", para);
                            //    break;
                    }

                return true;
            };
        }

        public IDisposable OpenMappedContext(string key, string value)
        {
            throw new NotImplementedException();
        }

        public IDisposable OpenNestedContext(string message)
        {
            throw new NotImplementedException();
        }
    }
}
