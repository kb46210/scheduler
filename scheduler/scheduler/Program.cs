﻿using Newtonsoft.Json;
using Quartz;
using Serilog;
using Serilog.Events;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Threading.Tasks;
using Quartz.Logging;
using JobLibrary.Share;
using Topshelf;
using System.Diagnostics;

namespace scheduler
{
    class Program
    {
        static readonly Dictionary<string, string> description = new Dictionary<string, string>() {
            { "Description","TSQDes" },
            { "DisplayName","TSQDis" },
            { "ServiceName","TSQSer"}
        };
        private static string thisFile = Process.GetCurrentProcess().MainModule.FileName;
        private static string basePath = AppDomain.CurrentDomain.BaseDirectory;
        static void Main(string[] args)
        {
            var topshelf = HostFactory.Run(x =>
            {
                x.Service<TSService>(s =>
                {
                    s.ConstructUsing(name => new TSService());
                    s.WhenStarted(tc => tc.Start());
                    s.WhenStopped(tc => tc.Stop());
                });
                x.RunAsLocalSystem();
                x.StartAutomatically();
                x.SetDescription(description["Description"]);
                x.SetDisplayName(description["DisplayName"]);
                x.SetServiceName(description["ServiceName"]);
                x.BeforeInstall(() => BeforeInstall());
                x.AfterUninstall(() => AfterUninstall());
            });
        }
        private static void BeforeInstall()
        {
            string startBash = $@"""{thisFile}"" start";
            File.WriteAllText(basePath + @"Start.bat", startBash);
            string stopBash = $@"""{thisFile}"" stop";
            File.WriteAllText(basePath + @"Stop.bat", stopBash);
            string[] restartBash = { $@"""{thisFile}"" stop", $@"""{thisFile}"" start" };
            File.WriteAllLines(basePath + @"Restart.bat", restartBash);
        }
        private static void AfterUninstall()
        {
            File.Delete(basePath + @"Start.bat");
            File.Delete(basePath + @"Stop.bat");
            File.Delete(basePath + @"Restart.bat");
        }
    }
}
