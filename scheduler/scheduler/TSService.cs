﻿using JobLibrary.Share;
using System;
using System.Collections.Generic;
using System.Text;

namespace scheduler
{
    class TSService
    {
        readonly Job jl;
        public TSService() {
            jl = new Job();
        }
        /// <summary>
        /// on service start
        /// </summary>
        public void Start() {
            jl.Start();
        }
        /// <summary>
        /// on service stop
        /// </summary>
        public void Stop() {
            jl.Stop();
        }
    }
}
